<?php
/**
 * WordPress の基本設定
 *
 * このファイルは、インストール時に wp-config.php 作成ウィザードが利用します。
 * ウィザードを介さずにこのファイルを "wp-config.php" という名前でコピーして
 * 直接編集して値を入力してもかまいません。
 *
 * このファイルは、以下の設定を含みます。
 *
 * * MySQL 設定
 * * 秘密鍵
 * * データベーステーブル接頭辞
 * * ABSPATH
 *
 * @link http://wpdocs.sourceforge.jp/wp-config.php_%E3%81%AE%E7%B7%A8%E9%9B%86
 *
 * @package WordPress
 */

// 注意: 
// Windows の "メモ帳" でこのファイルを編集しないでください !
// 問題なく使えるテキストエディタ
// (http://wpdocs.sourceforge.jp/Codex:%E8%AB%87%E8%A9%B1%E5%AE%A4 参照)
// を使用し、必ず UTF-8 の BOM なし (UTF-8N) で保存してください。

// ** MySQL 設定 - この情報はホスティング先から入手してください。 ** //
/** WordPress のためのデータベース名 */
define('DB_NAME', 'aa160lbgvp_wordpress0003');

/** MySQL データベースのユーザー名 */
define('DB_USER', 'aa160lbgvp');

/** MySQL データベースのパスワード */
define('DB_PASSWORD', 'xVX684k5');

/** MySQL のホスト名 */
define('DB_HOST', '127.0.0.1');

/** データベースのテーブルを作成する際のデータベースの文字セット */
define('DB_CHARSET', 'utf8mb4');

/** データベースの照合順序 (ほとんどの場合変更する必要はありません) */
define('DB_COLLATE', '');

/**#@+
 * 認証用ユニークキー
 *
 * それぞれを異なるユニーク (一意) な文字列に変更してください。
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org の秘密鍵サービス} で自動生成することもできます。
 * 後でいつでも変更して、既存のすべての cookie を無効にできます。これにより、すべてのユーザーを強制的に再ログインさせることになります。
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Rp{xgOz^8?F@QED6!M [lk*7%$NF,}[4:xv1/nZS/db<j^g=6iE=WL|s7btd9k7h');
define('SECURE_AUTH_KEY',  '*4xLK4R(-)>4gZQ3rk&N[~e8>&TOJW^uHTVSUNjk@,:FV)U@A-8ZJqNbW~l|NsK&');
define('LOGGED_IN_KEY',    'mw-M:GDf~`s{8WL!TH.6/IyiuqkYiZgejHIl;8q</0e9kj->G45[2{HR8H=jw@:T');
define('NONCE_KEY',        'E`a ~Z)+]pJC/R5^!F7,qk*xl|K^M7[y YWZR?=hwdRH?j^6P3I6N%Z}@qE,r_Y+');
define('AUTH_SALT',        '{e/#9e7A4WkT(e$PVGhp1!@TS>CAuEWgZzz5Qxnu*P:y1%ZN0e#ys&do+kkDif&6');
define('SECURE_AUTH_SALT', 'Nb04wU$6D-Ln?zPrZs?s$l`aQ_/g*&Dcv]. FMx1WFMeE>Cu*c:/o]y:)vUhFt(l');
define('LOGGED_IN_SALT',   '(&A0MVw!y^2e<EUk5#Lo!8vnE@-~}Xaep6#.~Lb/ShnTQ1wRQJ_:`gLb[D*j1Oi0');
define('NONCE_SALT',       'Kj}C_I18Hn/+oHRR:?0+m<?GtbYT60ra;BqJH-2hn5hGT4( @,^II/0N<(4!Cu;,');

/**#@-*/

/**
 * WordPress データベーステーブルの接頭辞
 *
 * それぞれにユニーク (一意) な接頭辞を与えることで一つのデータベースに複数の WordPress を
 * インストールすることができます。半角英数字と下線のみを使用してください。
 */
$table_prefix  = 'wp_';

/**
 * 開発者へ: WordPress デバッグモード
 *
 * この値を true にすると、開発中に注意 (notice) を表示します。
 * テーマおよびプラグインの開発者には、その開発環境においてこの WP_DEBUG を使用することを強く推奨します。
 *
 * その他のデバッグに利用できる定数については Codex をご覧ください。
 *
 * @link http://wpdocs.osdn.jp/WordPress%E3%81%A7%E3%81%AE%E3%83%87%E3%83%90%E3%83%83%E3%82%B0
 */
define('WP_DEBUG', false);

/* 編集が必要なのはここまでです ! WordPress でブログをお楽しみください。 */

/* define('RELOCATE', true);  /*間違ってWordpressアドレスを変更した時の対処

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
