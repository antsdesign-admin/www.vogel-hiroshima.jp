<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>

</div><!-- #content -->
</div><!-- .site-content-contain -->

<footer id="footer">
<div class="footer-inner">
<div class="logo">
	<a href="<?php bloginfo('url');?>/">
		<img src="<?php echo get_template_directory_uri(); ?>/img/common/logo_f.png" alt="立町法律事務所" />
	</a>
	<div class="address fmin"><span>広島市中区立町 2-25　IG 石田学園ビル 10F</span></div>
</div>
<div class="contact-wrapper">
<div class="contactBox">
	<div class="ttl"><span>お問合せ</span></div>
	<div class="tel"><span>TEL.</span><a href="tel:0822424510">082-242-4510</a></div>
	<div class="time"><span>受付時間／平日 9:00 ～ 18:00</span></div>
</div>
</div>
<div class="nav-wrapper">
<ul class="nav">
	<li><a href="<?php bloginfo('url');?>/">HOME</a></li>
<?php /*	<li><a href="<?php bloginfo('url');?>/information/">お知らせ</a></li>*/ ?>
	<li><a href="<?php bloginfo('url');?>/lawyers/">弁護士紹介</a></li>
	<li><a href="<?php bloginfo('url');?>/practices/">取扱い業務</a></li>
	<li><a href="<?php bloginfo('url');?>/costs/">費用について</a></li>
	<li><a href="<?php bloginfo('url');?>/#access">アクセス</a></li>
</ul>
</div>
<div class="copyright"><small>Copyright &copy; 2020 立町法律事務所 all rights reserved.</small></div>

<div id="pagetop">
	<a href="#page">PAGETOP</a>
	<?php /*?><a class="replace" href="#page">PAGETOP</a>*/?>
</div>
</div><!-- .wrap -->
</footer><!-- #colophon -->
</div><!-- #page -->
<?php wp_footer(); ?>

</body>
</html>
