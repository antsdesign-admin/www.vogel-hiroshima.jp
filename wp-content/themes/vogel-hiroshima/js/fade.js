// JavaScript Document

// スクロールでフェードイン
//$(function() {
//  'use strict';
//  var $fadein = $('.fadein');
//  $fadein.hide().fadeIn(3000);
//});


jQuery(function(){
    jQuery(window).scroll(function (){
        jQuery('.fadein').each(function(){
            var elemPos = jQuery(this).offset().top;
            var scroll = jQuery(window).scrollTop();
            var windowHeight = jQuery(window).height();
            if (scroll > elemPos - windowHeight + 400){
                jQuery(this).addClass('scrollin');
            }
        });
    });
  });