<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<title><?php wp_title( '|', true, 'right' ); ?></title>
<!--<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;">-->
<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/img/common/favicon.ico" />
<!--<link rel="icon" type="image/vnd.microsoft.icon" href="<?php echo get_template_directory_uri(); ?>/img/common/favicon.ico">-->
<link rel="stylesheet" href="https://use.typekit.net/vxt0gvy.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/common.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery-3.4.1.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/script.js"></script>
<?php if ( is_front_page() ): ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/index.css">
<?php elseif ( is_home() || is_single() ): ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/information.css">
<?php elseif ( is_page('lawyers') ): ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/lawyers.css">
<?php elseif ( is_page('practices') ): ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/practices.css">
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/fade.js"></script>
<?php elseif ( is_page('costs') ): ?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/costs.css">
<?php endif; ?>
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">

<header>
<div class="header-inner">
<h1 class="logo">
	<a href="<?php bloginfo('url');?>/">
		<img src="<?php echo get_template_directory_uri(); ?>/img/common/logo_h.png" alt="立町法律事務所" />
	</a>
</h1>
<div class="right-box">
	<div class="nav-wrapper">
	<ul class="nav fmin">
<?php /*		<li><a href="<?php bloginfo('url');?>/information/">お知らせ</a></li>*/ ?>
		<li><a href="<?php bloginfo('url');?>/lawyers/">弁護士紹介</a></li>
		<li><a href="<?php bloginfo('url');?>/practices/">取扱い業務</a></li>
		<li><a href="<?php bloginfo('url');?>/costs/">費用について</a></li>
		<li><a href="<?php bloginfo('url');?>/#access">アクセス</a></li>
	</ul>
	</div>
	<div class="contact-wrapper">
	<div class="contactBox">
		<div class="ttl fmin"><span>お問合せ</span></div>
		<div class="tel"><span>TEL.</span><a href="tel:0822424510">082-242-4510</a></div>
		<div class="time"><span>受付時間／平日9:00～18:00</span></div>
	</div>
	</div>
</div><!-- .right-box -->
</div><!-- .header-inner -->
</header><!-- header -->
<?php
// If a regular post or page, and not the front page, show the featured image.
if ( has_post_thumbnail() && ( is_single() || ( is_page() && ! twentyseventeen_is_frontpage() ) ) ) :
	echo '<div class="single-featured-image-header">';
	the_post_thumbnail( 'twentyseventeen-featured-image' );
	echo '</div><!-- .single-featured-image-header -->';
endif;
?>
<div class="site-content-contain">
	<div id="content" class="">
