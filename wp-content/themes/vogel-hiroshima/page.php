<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<?php
$page = get_post( get_the_ID() );
$slug = $page->post_name;
?>
<div id="<?php echo $slug; ?>_title" class="header_title">
	<div class="header_title_inner">
		<h2><span class="fgar"><?php echo $slug; ?></span><span class="fmin"><?php the_title(); ?></span></h2>
	</div>
	<div class="header_title_foot"></div>
</div>

<div class="wrap">
	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
		<?php if(have_posts()): while(have_posts()):the_post(); ?>
		<?php the_content(); ?>
		<?php endwhile; endif; ?>
		</main><!-- #main -->
	</div><!-- #primary -->
</div><!-- .wrap -->

<?php get_footer();?>
