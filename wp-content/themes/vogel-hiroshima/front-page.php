<?php
/**
 * The front page template file
 *
 * If the user has selected a static page for their homepage, this is what will
 * appear.
 * Learn more: https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage vogel-hiroshima
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>
<div id="primary" class="content-area">
<main id="main" class="site-main" role="main">
<div class="main-content">

	<div class="main-image"><?php /*<img src="<?php echo get_template_directory_uri(); ?>/img/index/main.jpg" alt="立町法律事務所" />*/?></div>

	<div class="main-copy">
	<h2 class="ttl fmin">法律を有効に用いて<br>あなたのお悩みを解決します。</h2>
	<div class="text">
	法律は、「法律を知らなかった」人を守ってくれません。<br>
	残念ながら「法律を知らなかった」人が損をしてしまうのが実情なのです。<br>
	しかし、それだけでも足りません。<br>
	きちんとした戦略がなければ、結果は運任せになります。<br>
	法律を正しく利用し、正しい戦略を立てることができれば、<br>
	これまで困難と思われていた問題が解決するかもしれません。<br>
	少なくとも、解決に向けた第一歩が踏み出せます。<br>
	弊事務所にお任せください。
	</div>
	</div><!-- .main-copy -->
</div><!-- .main-content -->
<div class="info-wrap">
	<h3 class="ttl fgar">Information</h3>
<?php /*	<div class="btn-wrap">
		<a href="<?php bloginfo('url');?>/information/" class="btn line">
			<span class="btn-ttl fmin">お知らせ</span>
			<span class="icon"></span>
		</a>
	</div>*/ ?>
	<div id="front_topics">
		<div id="topics_inner">
			<?php
			$the_query = new WP_Query( 'posts_per_page=2' );
			if ( $the_query->have_posts() ) :
				while ( $the_query->have_posts() ) :
				$the_query->the_post();
				$cat = get_the_category();
				$cat = $cat[0];
				$cat_name = $cat->name;
				//newアイコン出力
//				$days = 7; //何日間newをつけるか
//				$today = date_i18n('U');
//				$entry_day = get_the_time('U');
//				$keika = date('U',($today - $entry_day)) / 86400;
			?>
				<a href="<?php the_permalink(); ?>">
					<div class="topics_box">
<!--
						<div class="img_box">
							<?php if ( has_post_thumbnail()) { ?>
							<div class="img" style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);"></div>
								<?php } else { ?>
							<div class="img" style="background-image: url(<?php echo get_theme_file_uri( 'images/index/no_image.png' ); ?>);"></div>
								<?php } ?>
						</div>
-->
						<div class="topics_content_box">
							<div class="topics_icon">
<!--
								<?php if ( is_sticky()): ?>
								<p class="font_en recommend">Recommend!</p>
								<?php endif ?>
-->
<!--
								<?php if ( $days > $keika ): ?>
									<p class="font_en new">New!</p>
								<?php endif ?>
-->
							</div>
							<div class="topics_date">
								<span><?php echo get_post_time('Y.m.d'); ?></span>
							</div>
							<div class="topics_content">
								<div><?php the_title(); ?></div>
							</div>
						</div>
					</div>
				</a>
			<?php endwhile;
			endif;
			wp_reset_postdata();
			?>
		</div><!-- #topics_inner -->
	</div><!-- #front_topics -->
</div><!-- .info-wrap -->
<!------- lawyers ------->
<div class="box-two">
<div class="left">
	<a href="<?php bloginfo('url');?>/lawyers/" class="box-black-link">
		<div class="box-black-wrap">
			<h3 class="ttl fgar">Lawyers</h3>
			<div class="box-black lawyers"></div>
		</div>
	</a>
</div>
<div class="right">
	<div class="btn-wrap">
		<a href="<?php bloginfo('url');?>/lawyers/" class="btn line">
			<span class="btn-ttl fmin">弁護士紹介</span>
			<span class="icon"></span>
		</a>
	</div>
</div>
</div><!-- .box-two -->
<!------- lawyers ------->
<!------- practice ------->
<div class="ttl-wrap">
	<h3 class="ttl fgar">Practices</h3>
	<div class="btn-wrap">
		<a href="<?php bloginfo('url');?>/practices/" class="btn">
			<span class="btn-ttl fmin">取扱い業務</span>
			<span class="icon"></span>
		</a>
	</div>
</div>
<div class="box-three mb54">
<a href="<?php bloginfo('url');?>/practices/#traffic" class="box-black-link">
	<div class="box-black-wrap">
		<div class="ttl">
			<h4 class="fgar">交通事故トラブル</h4>
			<div class="icon-wrap"><div class="icon"></div></div>
		</div>
		<div class="box-black p-traffic"></div>
	</div>
</a>
<a href="<?php bloginfo('url');?>/practices/#inheritance" class="box-black-link">
	<div class="box-black-wrap">
		<div class="ttl">
			<h4 class="fgar">相 続</h4>
			<div class="icon-wrap"><div class="icon"></div></div>
		</div>
		<div class="box-black p-inheritance"></div>
	</div>
</a>
<a href="<?php bloginfo('url');?>/practices/#company" class="box-black-link">
	<div class="box-black-wrap">
		<div class="ttl">
			<h4 class="fgar">企業法務・顧問契約</h4>
			<div class="icon-wrap"><div class="icon"></div></div>
		</div>
		<div class="box-black p-company"></div>
	</div>
</a>
<a href="<?php bloginfo('url');?>/practices/#divorce" class="box-black-link">
	<div class="box-black-wrap">
		<div class="ttl">
			<h4 class="fgar">離 婚</h4>
			<div class="icon-wrap"><div class="icon"></div></div>
		</div>
		<div class="box-black p-divorce"></div>
	</div>
</a>
<a href="<?php bloginfo('url');?>/practices/#criminal" class="box-black-link">
	<div class="box-black-wrap">
		<div class="ttl">
			<h4 class="fgar">刑事事件</h4>
			<div class="icon-wrap"><div class="icon"></div></div>
		</div>
		<div class="box-black p-criminal"></div>
	</div>
</a>
<a href="<?php bloginfo('url');?>/practices/#etc" class="box-black-link">
	<div class="box-black-wrap">
		<div class="ttl">
			<h4 class="fgar">その他の問題</h4>
			<div class="icon-wrap"><div class="icon"></div></div>
		</div>
		<div class="box-black p-etc"></div>
	</div>
</a>
</div><!-- .box-wh -->
<!------- practice ------->
<!------- expence ------->
<div class="box-one">
<a href="<?php bloginfo('url');?>/expense/" class="box-black-link">
	<div class="box-black-wrap">
		<div class="ttl">
		<h3 class="ttl fgar">Expense</h3>
			<h4 class="fgar">費用について</h4>
			<div class="icon-wrap"><div class="icon"></div></div>
		</div>
		<div class="box-black expense"></div>
	</div>
</a>
</div><!-- .box-one -->
<!------- expence ------->
<!------- practice ------->
<div class="ttl-wrap" id="access">
	<h3 class="ttl fgar">Access</h3>
	<div class="text">広島電鉄本線 立町駅下車、紙屋町方面へ徒歩2分 りそな銀行西隣</div>
	<div class="mapimage"><a href="https://goo.gl/maps/xsjRcbUWce8rAy7RA" target="_blank" class="map"><img src="<?php echo get_template_directory_uri(); ?>/img/common/img_access.jpg" alt="立町法律事務所" /></a></div>
</div>
<!--<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3292.27831208921!2d132.4581076155848!3d34.39427130707478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x355aa20f2af6ee61%3A0xc0b282fa30343bb4!2z44CSNzMwLTAwMzIg5bqD5bO255yM5bqD5bO25biC5Lit5Yy656uL55S677yS4oiS77yS77yVIOW6g-Wztue1jOa4iOWkp-WtpiAxMEY!5e0!3m2!1sja!2sjp!4v1566204814801!5m2!1sja!2sjp" width="100%" height="320" frameborder="0" style="border:0" allowfullscreen class="map"></iframe>-->
<!------- practice ------->
</main><!-- #main -->
</div><!-- #primary -->

<?php get_footer(); ?>
