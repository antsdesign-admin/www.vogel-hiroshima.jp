<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage vogel-hiroshima
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="information_title" class="header_title">
	<div class="header_title_inner">
		<h2><span class="fgar">Information</span><span class="fmin">お知らせ</span></h2>
	</div>
	<div class="header_title_foot"></div>
</div>
<div id="page_topics">
	<div class="topics-wrapper">
		<div class="clear">
		<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
			<?php if(function_exists('bcn_display'))
			{
				bcn_display();
			}?>
		</div>
		</div>
		<ul class="topics-box">
		<?php if(have_posts()): while (have_posts()) : the_post();?>
			<li>
				<a href="<?php the_permalink();?>">
<!--

					<div class="img_box">
						<?php if ( has_post_thumbnail()) { ?>
						<div class="img" style="background-image: url(<?php the_post_thumbnail_url( 'full' ); ?>);"></div>
							<?php } else { ?>
						<div class="img" style="background-image: url(<?php echo get_theme_file_uri( 'images/front/no_image.png' ); ?>);"></div>
							<?php } ?>
						<div class="img">
							<?php if ( has_post_thumbnail() ) { the_post_thumbnail('large'); }else{?><img src="<?php echo get_template_directory_uri(); ?>/images/front/no_image.png" alt=""><?php } ?>
						</div>
					</div>
-->
					<div class="topics_content_box">
						<div class="date"><?php echo get_post_time('Y.m.d'); ?></div>
						<div class="content"><?php the_title(); ?></div>
					</div>
				</a>
			</li>
		<?php endwhile; endif;?>
		</ul>
		<?php if(function_exists('wp_pagenavi')) { wp_pagenavi(
			array(
				'options' => array( 
//					'prev_text' => " ",
//					'next_text' => " ",
				),
			)
		); }?>
	</div>
</div>


<?php get_footer();
