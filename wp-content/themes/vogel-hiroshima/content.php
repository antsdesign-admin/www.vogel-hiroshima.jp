<?php 
	$cat = get_the_category();
	$cat = $cat[0];
	$cat_name = $cat->name;
	$cat_slug = $cat->slug;
?>
<article class="event_content">

<div class="entry_header">
	<div class="entry_inner">
		<p class="posted_date"><?php echo get_post_time('Y.m.d D'); ?><?php /* echo $cat_name; */?></p>
		<?php if ( is_single() ){ ?>
			<h2 class="posted_title"><?php the_title(); ?></h2>
		<?php } else { ?>
			<h2 class="posted_title">
				<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
			</h2>
		<?php } ?>
	</div>
</div>
<div class="entry_content entry_inner">
	<?php the_content(); ?>
</div>

<footer class="entry_footer">
<?php /*
	<nav class="nav-single">
		<span class="nav_arc"><a href="<?php echo esc_url( home_url( '/' ) ); ?>information/">一覧へ</a></span>
	</nav>
	</ul>
*/ ?>
<?php /*
	<ul id="entry_sns">
		<?php
			$url_encode=urlencode(get_permalink());
			$title_encode=urlencode(get_the_title());
		?>
		<li class="facebook">
			<a href="http://www.facebook.com/sharer.php?src=bm&u=<?php echo $url_encode;?>&t=<?php echo $title_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;">
				<i class="fa fa-facebook-square"></i>
				<?php if(function_exists('scc_get_share_facebook')) echo (scc_get_share_facebook()==0)?'':scc_get_share_facebook(); ?>
			</a>
		</li>
	</ul>
*/ ?>
</footer>
</article>
