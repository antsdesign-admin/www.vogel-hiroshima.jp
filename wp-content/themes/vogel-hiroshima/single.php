<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

get_header(); ?>

<div id="information_title" class="header_title">
	<div class="header_title_inner">
		<h2><span class="fgar">Information</span><span class="fmin">お知らせ</span></h2>
	</div>
	<div class="header_title_foot"></div>
</div>
<div id="page_topics">
<div class="wrapper">
<div id="main_container">
	<div class="clear">
	<div class="breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
		<?php if(function_exists('bcn_display'))
		{
			bcn_display();
		}?>
	</div>
	</div>
	<?php while ( have_posts() ) : the_post(); ?>
<?php /*				<?php get_template_part( 'content' ); ?> */ ?><!--content.phpをテンプレートにする-->
	<?php 
		$cat = get_the_category();
		$cat = $cat[0];
		$cat_name = $cat->name;
		$cat_slug = $cat->slug;
	?>
	<article class="event_content">
	<div class="entry_header">
		<div class="entry_inner">
			<p class="posted_date"><?php echo get_post_time('Y.m.d'); ?><?php /* echo $cat_name; */?></p>
			<?php if ( is_single() ){ ?>
				<h3 class="posted_title"><?php the_title(); ?></h3>
			<?php } else { ?>
				<h3 class="posted_title">
					<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
				</h3>
			<?php } ?>
		</div>
	</div>
	<div class="entry_content entry_inner">
		<?php the_content(); ?>
	</div>
	<div class="entry_footer">
	<?php /*
		<nav class="nav-single">
			<span class="nav_arc"><a href="<?php echo esc_url( home_url( '/' ) ); ?>information/">一覧へ</a></span>
		</nav>
		</ul>
	*/ ?>
	<?php /*
		<ul id="entry_sns">
			<?php
				$url_encode=urlencode(get_permalink());
				$title_encode=urlencode(get_the_title());
			?>
			<li class="facebook">
				<a href="http://www.facebook.com/sharer.php?src=bm&u=<?php echo $url_encode;?>&t=<?php echo $title_encode;?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=500');return false;">
					<i class="fa fa-facebook-square"></i>
					<?php if(function_exists('scc_get_share_facebook')) echo (scc_get_share_facebook()==0)?'':scc_get_share_facebook(); ?>
				</a>
			</li>
		</ul>
	*/ ?>
	</div>
	</article>
	<nav class="nav-single clear">
		<span class="nav-next"><?php previous_post_link( '%link', '<span class="meta-nav">' . '＜　前の記事へ'. '</span>' ); ?></span>
<?php /*		<span class="nav_arc">｜<a href="<?php echo esc_url( home_url( '/' ) ); ?>information/">一覧へ戻る</a>｜</span>*/ ?>
		<span class="nav-previous"><?php next_post_link( '%link', '<span class="meta-nav">' . '次の記事へ　＞' . '</span>' ); ?></span>
	</nav>
	<?php endwhile; ?>
</div><!--#main_container-->
</div><!--.wrapper-->
</div><!--#page_topics-->

<?php get_footer();
