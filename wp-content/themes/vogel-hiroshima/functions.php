<?php

// パンくず 固定ページ用
function short_php($params = array()) {
extract(shortcode_atts(array(
'file' => 'default'
), $params));
ob_start();
include(STYLESHEETPATH . "/$file.php");
return ob_get_clean();
}
 
add_shortcode('myphp', 'short_php');


//remove_filter('the_content', 'wpautop');　// 記事の自動整形を無効にする
?>
